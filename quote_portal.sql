
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE IF NOT EXISTS `quotes` (
  `quote_link` varchar(255) NOT NULL,
  `quote` varchar(1000) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `uploaded` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `quotes`
 ADD PRIMARY KEY (`quote_link`);

