<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 24/5/17
 * Time: 2:34 PM
 */
require_once('vendor/autoload.php');

function tweet($message, $image)
{

// add the codebird library


// note: consumerKey, consumerSecret, accessToken, and accessTokenSecret all come from your twitter app at https://apps.twitter.com/
    \Codebird\Codebird::setConsumerKey("jhWZWdr2cf3rJlyHR6dBGafCU", "VYCDESdy5ukeqxdOiAw4KSDBZyjHy1OA1obGcYxsXdBt3YuZ2p");
    $cb = \Codebird\Codebird::getInstance();

    $cb->setToken("756131328114896897-3jkTplfI7t3gAAnrADhOVGoXEzwnDXK", "uxldZaDfAss1OvTvngNY5tzMD5tJ08bqgsgEK5mJMLdKu");









//build an array of images to send to twitter
    $reply = $cb->media_upload(array(
        'media' => $image
    ));
//upload the file to your twitter account
    $mediaID = $reply->media_id_string;

//build the data needed to send to twitter, including the tweet and the image id
    $params = array(
        'status' => $message,
        'media_ids' => $mediaID
    );
//post the tweet with codebird
    $reply = $cb->statuses_update($params);
    print "Upload done";

}

// the function is called in scrapQuote.php
function run()
{

    $conn = new PDO('mysql:host=localhost;dbname=quote_portal;charset=utf8', 'root', 'snpo', array(PDO::ATTR_EMULATE_PREPARES => false, PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
    $stmt = $conn->prepare("SELECT quote_link, quote, author FROM quotes WHERE uploaded = '0'");
    // monkeylearn
    $token = '9e87cc5ce2fa88a3169c3b886ffb54e779bbcfa5';
    $client = new Artstorm\MonkeyLearn\Client($token);

    if ($stmt->execute()) {
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            upload($row['quote_link'], $row['quote'], $row['author'], $conn,$client);
        }
    }
}

//uploading the quote image to twitter with author name
function upload($quote_link, $quote, $author, $conn,$client)
{
    $emojiset=[];


    $happy=[
        '&#x1F601;',
        '&#x1F602;',
        '&#x1F603;',
        '&#x1F604;',
        '&#x1F605;',
        '&#x1F606;',
        '&#x1F609;',
        '&#x1F60A;',
        '&#x1F60C;'

    ];

    $neutral=[
        '&#x1F610;',
        '&#x1F611;',
        '&#x1F636;',
        '&#x1F644;',
    ];

    $sad=[
        '&#x1F612;',
        '&#x1F613;',
        '&#x1F614;',
        '&#x1F61E;',
        '&#x1F622;',
        '&#x1F623;',
        '&#x1F625;',
        '&#x1F629;',
        '&#x1F62A;'
    ];
    $uploaded = 1;
    try {
        $textToClassify=array($quote);
        $module = 'cl_qkjxv9Ly';
        $response = $client->classification->classify($textToClassify, $module);
        if($response->result()[0][0]['label']=="positive"){
            $emojiset=$happy;
        }
        else if($response->result()[0][0]['label']=="negative"){
            $emojiset=$sad;
        }
        else if($response->result()[0][0]['label']=="neutral"){

            $emojiset=$neutral;
        }
        tweet(html_entity_decode($emojiset[rand(0,count($emojiset)-1)])." \n-".$author, $quote_link);
    } catch (Exception $e) {
        print("Quotes will upload on next run");
        exit(0);

    }
    //setting up "uploaded" attribute to 1 if uploaded
    $stmt = $conn->prepare("UPDATE quotes SET uploaded=:uploaded WHERE quote_link=:quote_link");
    $stmt->bindValue(":uploaded", $uploaded);
    $stmt->bindValue(":quote_link", $quote_link);
    $stmt->execute();
    echo "Quote uploaded:::::" . $quote . "\n";
}

