<?php
set_time_limit(300);

$rows = [];
$url = 'https://www.brainyquote.com/quotes/quote_pictures.html';
print("::::::::Scrapping quotes::::::::\n");
//initializing curl
$curl = curl_init($url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
curl_setopt($curl, CURLOPT_HTTPHEADER, array('User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:19.0) Gecko/20100101 Firefox/19.0'));
curl_setopt($curl, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5);
$page = curl_exec($curl);

if (curl_errno($curl)) {
    echo 'Scraper error: ' . curl_error($curl);
    exit;
}

curl_close($curl);
//curl closed


//Simple dom HTML
$doc = new DOMDocument;
libxml_use_internal_errors(true);
if (!$doc->loadHTML($page)) {
    $errors = "";
    foreach (libxml_get_errors() as $error) {
        $errors .= $error->message . "<br/>";
    }

    libxml_clear_errors();
    print "libxml errors:<br>$errors";
    return;
}

//using xpath to scrap data
$xpath = new DOMXPath($doc);
$elements = $xpath->query("//div[contains(@class,'m-brick grid-item boxy bqQt')]");
$site = "http://www.brainyquote.com";
if (!is_null($elements)) {
    foreach ($elements as $element) {
        global $rows;
        global $categoryList;
        $document = new DOMDocument();
        $cloned = $element->cloneNode(TRUE);
        $document->appendChild($document->importNode($cloned, True));
        $xpath = new DOMXPath($document);

        //scrapping quote link
        $quote_link = $xpath->query("//a/img/@src");
        $quote_link = $site . $quote_link->item(0)->nodeValue;


        //scrapping quote
        $quote = $xpath->query("//a[contains(@title,'view quote')]");
        $quote = $quote->item(0)->nodeValue;

        //scrapping author
        $author = $xpath->query("//a[contains(@title,'view author')]");
        $author = $author->item(0)->nodeValue;

        $row = array(
            'quote_link' => $quote_link,
            'quote' => $quote,
            'author' => $author,
            'url_update' => $quote_link
        );
        array_push($rows, $row);
    }
    print "::::::::Scrapping Complete::::::::\n Updating quotes to Twitter\n";
    insertData($rows);
}


//function to insert data to verify if it us uploaded or not on twitter using attribute "uploaded"
function insertData($rows)
{

    $conn = new PDO('mysql:host=localhost;dbname=quote_portal;charset=utf8', 'root', 'snpo', array(PDO::ATTR_EMULATE_PREPARES => false, PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
    $sql = "INSERT INTO quotes (quote_link,quote,author) VALUES (:quote_link,:quote,:author) ON DUPLICATE KEY UPDATE quote_link = :url_update";

    try {
        $conn->beginTransaction();
        $stmt = $conn->prepare($sql);
        foreach ($rows as $row) {
            // now loop through each inner array to match bound values
            foreach ($row as $column => $value) {
                $stmt->bindValue(":{$column}", $value, PDO::PARAM_STR);
            }
            $stmt->execute();

        }
        $conn->commit();
        //echo "Inserted for brain quotes";
    } catch (PDOException $e) {
        $conn->rollBack();
        echo 'ERROR: ' . $e->getMessage();
    }
}

//running to Twitter file
require 'toTwitter.php';
run();


